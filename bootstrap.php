<?php

// Constantes de chemins
define ('ROOT_PATH', __DIR__);
define ('PUBLIC_PATH', ROOT_PATH . DIRECTORY_SEPARATOR . 'public');
define ('LIB_PATH', ROOT_PATH . DIRECTORY_SEPARATOR . 'lib');
define ('DATA_PATH', ROOT_PATH . DIRECTORY_SEPARATOR . 'data');
define ('APP_PATH', ROOT_PATH . DIRECTORY_SEPARATOR . 'app');
define ('LOG_PATH', ROOT_PATH . DIRECTORY_SEPARATOR . 'log');
define ('CACHE_PATH', ROOT_PATH . DIRECTORY_SEPARATOR . 'cache');

define ('MINZ_PATH', LIB_PATH . DIRECTORY_SEPARATOR . 'Minz');
define ('DATABASE_FILE', DATA_PATH . DIRECTORY_SEPARATOR . 'db.sqlite');

set_include_path (get_include_path ()
                 . PATH_SEPARATOR
                 . LIB_PATH
                 . PATH_SEPARATOR
                 . MINZ_PATH
                 . PATH_SEPARATOR
                 . APP_PATH);

require (APP_PATH . DIRECTORY_SEPARATOR . 'App_FrontController.php');
