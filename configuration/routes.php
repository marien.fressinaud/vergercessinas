<?php

return array (
	// index
	array (
		'route'       => '/(\d+)-(.+).html',
		'controller'  => 'index',
		'action'      => 'index',
		'params'      => array ('page', 'title')
	),
	
	// diapo
	array (
		'route'       => '/diaporama/?',
		'controller'  => 'galerie',
		'action'      => 'index',
	),
	array (
		'route'       => '/diaporama/(\d+)_(\d+)-(.+).html',
		'controller'  => 'galerie',
		'action'      => 'see',
		'params'      => array('gal', 'photo', 'title')
	),
	array (
		'route'       => '/diaporama/(\d+)-(.+).html',
		'controller'  => 'galerie',
		'action'      => 'see',
		'params'      => array('gal', 'title')
	),

	// admin
	array (
		'route'       => '/admin.php',
		'controller'  => 'admin',
		'action'      => 'index',
	),
	array (
		'route'       => '/admin/user.php',
		'controller'  => 'admin',
		'action'      => 'user',
	),
	array (
		'route'       => '/admin/login.php',
		'controller'  => 'admin',
		'action'      => 'login',
	),
	array (
		'route'       => '/admin/logout.php',
		'controller'  => 'admin',
		'action'      => 'logout',
	),
	
	// admin news
	array (
		'route'       => '/admin/news.php',
		'controller'  => 'admin_news',
		'action'      => 'index',
	),
	array (
		'route'       => '/admin/news.php',
		'controller'  => 'admin_news',
		'action'      => 'index',
	),
	array (
		'route'       => '/admin/news/add.php',
		'controller'  => 'admin_news',
		'action'      => 'add',
	),
	array (
		'route'       => '/admin/news/update.php',
		'controller'  => 'admin_news',
		'action'      => 'update',
	),
	array (
		'route'       => '/admin/news/delete.php',
		'controller'  => 'admin_news',
		'action'      => 'delete',
	),

	// admin page
	array (
		'route'       => '/admin/page.php',
		'controller'  => 'admin_page',
		'action'      => 'index',
	),
	array (
		'route'       => '/admin/page/add.php',
		'controller'  => 'admin_page',
		'action'      => 'add',
	),
	array (
		'route'       => '/admin/page/update.php',
		'controller'  => 'admin_page',
		'action'      => 'update',
	),
	array (
		'route'       => '/admin/page/add_line.php',
		'controller'  => 'admin_page',
		'action'      => 'add_line',
	),
	array (
		'route'       => '/admin/page/add_block.php',
		'controller'  => 'admin_page',
		'action'      => 'add_block',
	),
	array (
		'route'       => '/admin/page/delete.php',
		'controller'  => 'admin_page',
		'action'      => 'delete',
	),

	// admin page block
	array (
		'route'       => '/admin/page_block/update.php',
		'controller'  => 'admin_page_block',
		'action'      => 'update',
	),
	array (
		'route'       => '/admin/page_block/delete.php',
		'controller'  => 'admin_page_block',
		'action'      => 'delete',
	),

	// admin lien
	array (
		'route'       => '/admin/lien.php',
		'controller'  => 'admin_lien',
		'action'      => 'index',
	),
	array (
		'route'       => '/admin/lien/add.php',
		'controller'  => 'admin_lien',
		'action'      => 'add',
	),
	array (
		'route'       => '/admin/lien/update.php',
		'controller'  => 'admin_lien',
		'action'      => 'update',
	),
	array (
		'route'       => '/admin/lien/delete.php',
		'controller'  => 'admin_lien',
		'action'      => 'delete',
	),
	
	// admin diapo
	array (
		'route'       => '/admin/galerie.php',
		'controller'  => 'admin_galerie',
		'action'      => 'index',
	),
	array (
		'route'       => '/admin/galerie/add.php',
		'controller'  => 'admin_galerie',
		'action'      => 'add',
	),
	array (
		'route'       => '/admin/galerie/update.php',
		'controller'  => 'admin_galerie',
		'action'      => 'update',
	),
	array (
		'route'       => '/admin/galerie/delete.php',
		'controller'  => 'admin_galerie',
		'action'      => 'delete',
	),
	array (
		'route'       => '/admin/galerie/check.php',
		'controller'  => 'admin_galerie',
		'action'      => 'check_galerie',
	),
	array (
		'route'       => '/admin/galerie/upload_photo.php',
		'controller'  => 'admin_galerie',
		'action'      => 'upload_photo',
	),
	
	// admin photo
	array (
		'route'       => '/admin/galerie/photo.php',
		'controller'  => 'admin_photo',
		'action'      => 'index',
	),
	array (
		'route'       => '/admin/galerie/photo/update.php',
		'controller'  => 'admin_photo',
		'action'      => 'update',
	),
	array (
		'route'       => '/admin/galerie/photo/delete.php',
		'controller'  => 'admin_photo',
		'action'      => 'delete',
	),
);
