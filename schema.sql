CREATE TABLE `article` (
  `idArticle` integer NOT NULL PRIMARY KEY AUTOINCREMENT
,  `titleArticle` varchar(50) NOT NULL
,  `contentArticle` text NOT NULL
,  `dateArticle` integer NOT NULL
,  `levelArticle` integer NOT NULL
);
CREATE TABLE `galerie` (
  `idGalerie` integer NOT NULL PRIMARY KEY AUTOINCREMENT
,  `nomGalerie` varchar(50) NOT NULL
,  `descriptionGalerie` text NOT NULL
,  `pathGalerie` varchar(250) NOT NULL
,  UNIQUE (`pathGalerie`)
);
CREATE TABLE `lien` (
  `idLien` integer NOT NULL PRIMARY KEY AUTOINCREMENT
,  `urlLien` varchar(150) NOT NULL
,  `libelleLien` varchar(100) NOT NULL
);
CREATE TABLE `page` (
  `idPage` integer NOT NULL PRIMARY KEY AUTOINCREMENT
,  `titlePage` varchar(50) NOT NULL
,  `aliasPage` varchar(20) NOT NULL
,  `contentPage` text NOT NULL
,  `datePage` integer NOT NULL
,  `locPage` integer NOT NULL
,  `layoutPage` varchar(50) NOT NULL DEFAULT "0"
);
CREATE TABLE `pageBlock` (
  `idPageBlock` integer NOT NULL PRIMARY KEY AUTOINCREMENT
,  `contentPageBlock` text NOT NULL
,  `orderPageBlock` integer NOT NULL
,  `idPage` integer NOT NULL
,  CONSTRAINT `pageBlock_ibfk_1` FOREIGN KEY (`idPage`) REFERENCES `page` (`idPage`)
);
CREATE TABLE `photo` (
  `idPhoto` integer NOT NULL PRIMARY KEY AUTOINCREMENT
,  `filePhoto` varchar(100) NOT NULL
,  `titrePhoto` varchar(100) NOT NULL
,  `descriptionPhoto` text NOT NULL
,  `ordrePhoto` integer NOT NULL
,  `idGalerie` integer NOT NULL
,  CONSTRAINT `photo_ibfk_1` FOREIGN KEY (`idGalerie`) REFERENCES `galerie` (`idGalerie`)
);
CREATE TABLE `user` (
  `loginUser` varchar(15) NOT NULL
,  `mailUser` varchar(50) DEFAULT NULL
,  `passUser` varchar(50) NOT NULL
,  UNIQUE (`loginUser`)
,  UNIQUE (`loginUser`)
);
CREATE INDEX "idx_photo_idGalerie" ON "photo" (`idGalerie`);
