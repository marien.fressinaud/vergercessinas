<?php
/** 
 * MINZ - Copyright 2011 Marien Fressinaud
 * Sous licence AGPL3 <http://www.gnu.org/licenses/>
*/
require ('FrontController.php');

class App_FrontController extends FrontController {
	public function init () {
		Session::init ();

		$this->initLib ();
		$this->initModels ();
		$this->initStylesAndScripts ();
		$this->initDB();
		$this->initFirstData ();
		$this->initView ();
	}

	private function initLib () {
		include (LIB_PATH . '/Parsedown.php');
		include (LIB_PATH . '/ParsedownExtra.php');
		include (LIB_PATH . '/verger/filter.php');
	}

	private function initModels () {
		include (APP_PATH . '/models/Article.php');
		include (APP_PATH . '/models/Galerie.php');
		include (APP_PATH . '/models/Lien.php');
		include (APP_PATH . '/models/Page.php');
		include (APP_PATH . '/models/PageBlock.php');
		include (APP_PATH . '/models/Photo.php');
		include (APP_PATH . '/models/User.php');
	}

	private function initStylesAndScripts () {
		View::appendStyle (Url::display ('/themes/default/font-awesome.min.css'));
	}

	private function initView () {
		$pageDAO = new PageDAO ();
		View::_param ('pagesHeader', $pageDAO->listerByLocation (Page::POS_HEADER));
		View::_param ('pagesMenu', $pageDAO->listerByLocation (Page::POS_MENU));
		View::_param ('pagesFooter', $pageDAO->listerByLocation (Page::POS_FOOTER));

		$lienDAO = new LienDAO ();
		View::_param ('liensMenu', $lienDAO->lister ());

		$articleDAO = new ArticleDAO ();
		View::_param ('news', $articleDAO->searchLastArticle ());
	}

	private function initDB () {
		$pdo = new Model_pdo();
		if (!$pdo->isInitialized()) {
			$schemaFilename = ROOT_PATH . DIRECTORY_SEPARATOR . 'schema.sql';
			if (file_exists($schemaFilename)) {
				$schema = file_get_contents($schemaFilename);
				$pdo->init($schema);
			}
		}
	}

	private function initFirstData () {
		// Init the first (and unique?) user
		$userDAO = new UserDAO ();
		$firstUser = $userDAO->searchMainUser ();
		if (!$firstUser) {
			// oops, no user! Let's create a default one!
			// And yes, the security of this is really poor, I cry now...
			$userDAO->createUser('admin', sha1('my-secret'));
		}

		// Init the default page
		$pageDAO = new PageDAO ();
		$defaultPage = $pageDAO->searchMainPage();
		if (!$defaultPage) {
			$pageDAO->addDefaultPage();
		}
	}
}
