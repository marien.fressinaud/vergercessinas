<?php

class PageBlock extends Model {
	private $id;
	private $idPage;
	private $content;
	private $order;

	public function id () { return $this->id; }
	public function idPage () { return $this->idPage; }
	public function content () { return $this->content; }
	public function mdContent () {
		$Parsedown = new ParsedownExtra();
		$Parsedown->setBreaksEnabled(true);
		return $Parsedown->text($this->content);
	}
	public function order () { return $this->order; }

	public function _id ($id) { $this->id = $id; }
	public function _idPage ($id) { $this->idPage = $id; }
	public function _content ($content) { $this->content = $content; }
	public function _order ($order) { $this->order = $order; }
}

class PageBlockDAO extends Model_pdo {
	public function __construct() {
		parent::__construct();
	}

	public function searchById ($id) {
		$sql = 'SELECT * FROM pageBlock WHERE idPageBlock=?';
		$values = array ($id);
		$stm = $this->bd->prepare ($sql);
		$stm->execute ($values);
		$res = $stm->fetchAll (PDO::FETCH_CLASS);

		if (!empty ($res)) {
			return HelperPageBlock::daoToPageBlock ($res[0]);
		} else {
			return false;
		}
	}

	public function listByPage($idPage) {
		$sql = 'SELECT * FROM pageBlock WHERE idPage=? ORDER BY orderPageBlock';
		$stm = $this->bd->prepare ($sql);
		$stm->execute (array($idPage));

		return HelperPageBlock::listDaoToPageBlock ($stm->fetchAll (PDO::FETCH_CLASS));
	}

	public function insertBlockAt($idPage, $order) {
		$sql = 'UPDATE pageBlock SET orderPageBlock = orderPageBlock + 1 WHERE idPage = ? AND ? <= orderPageBlock';
		$stm = $this->bd->prepare ($sql);
		if (!$stm->execute (array($idPage, $order))) {
			return false;
		}

		$sql = 'INSERT INTO pageBlock(contentPageBlock, orderPageBlock, idPage) VALUES(?, ?, ?)';
		$statement = $this->bd->prepare($sql);
		if (!$statement->execute(array('', $order, $idPage))) {
			return false;
		}

		return $this->bd->lastInsertId();
	}

	public function update ($id, $valuesTmp) {
		$sql = 'UPDATE pageBlock SET contentPageBlock=? WHERE idPageBlock=?';
		$stm = $this->bd->prepare ($sql);

		$values = array (
			$valuesTmp['contentPageBlock'],
			$id
		);

		if ($stm && $stm->execute ($values)) {
			return true;
		} else {
			return false;
		}
	}

	public function delete ($block) {
		$sql = 'UPDATE pageBlock SET orderPageBlock = orderPageBlock - 1 WHERE idPage = ? AND ? < orderPageBlock';
		$stm = $this->bd->prepare ($sql);
		if (!$stm->execute (array($block->idPage(), $block->order()))) {
			return false;
		}

		$sql = 'DELETE FROM pageBlock WHERE idPageBlock=?';
		$stm = $this->bd->prepare ($sql);
		$values = array ($block->id());

		if ($stm && $stm->execute ($values)) {
			return true;
		} else {
			return false;
		}
	}
}

class HelperPageBlock {
	public static function daoToPageBlock ($dao) {
		if (!is_array ($dao)) {
			$dao = array ($dao);
		}

		$pages = HelperPageBlock::listDaoToPageBlock ($dao);

		return $pages[0];
	}

	public static function listDaoToPageBlock ($listeDAO) {
		$liste = array ();

		foreach ($listeDAO as $key => $dao) {
			$liste[$key] = new PageBlock ();
			$liste[$key]->_id ($dao->idPageBlock);
			$liste[$key]->_idPage ($dao->idPage);
			$liste[$key]->_content ($dao->contentPageBlock);
			$liste[$key]->_order ($dao->orderPageBlock);
		}

		return $liste;
	}
}
