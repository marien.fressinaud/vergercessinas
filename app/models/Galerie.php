<?php

class Galerie extends Model {
	private $id;
	private $path; // chemin à partir du répertoire public
	private $nom;
	private $description;
	private $photos = array (); // photos récupérées à partir du $path
	private $photosBD = array (); // photos en bd

	public function __construct ($nom, $path, $id = 0) {
		$this->nom = $nom;
		$this->path = $path;
		if ($id > 0) {
			$this->id = $id;
		}
		$this->loadPhotos ();
		$this->loadPhotosBD ();

		if (!is_dir (PUBLIC_PATH . $this->path)) {
			mkdir (PUBLIC_PATH . $this->path, 0777, true);
		}
		if (!is_dir (PUBLIC_PATH . $this->path . '/thumbs')) {
			mkdir (PUBLIC_PATH . $this->path . '/thumbs');
		}
		if (!is_dir (PUBLIC_PATH . $this->path . '/resized')) {
			mkdir (PUBLIC_PATH . $this->path . '/resized');
		}

		//chmod (PUBLIC_PATH . $this->path, 0777);
		//chmod (PUBLIC_PATH . $this->path . '/thumbs', 0777);
		//chmod (PUBLIC_PATH . $this->path . '/resized', 0777);
	}

	public function id () { return $this->id; }
	public function path () { return $this->path; }
	public function nom () { return $this->nom; }
	public function description () { return $this->description; }
	public function photos () { return $this->photos; }
	public function photosBD () { return $this->photosBD; }
	public function _id ($id) { $this->id = $id; }
	public function _path ($path) { $this->path = $path; }
	public function _nom ($nom) { $this->nom = $nom; }
	public function _description ($description) { $this->description = $description; }
	public function _photos ($photos) { $this->photos = $photos; }
	public function _photosBD ($photosBD) { $this->photosBD = $photosBD; }

	// vérifie si, pour chaque photo de la galerie, il existe une miniature et une redimensionnée
	// @param $make si à true, créé automatiquement la miniature
	// @return true si toutes les photos ont leur miniature, false sinon
	public function checkResized ($make = true) {
		$ok = true;

		foreach ($this->photos as $photo) {
			if (!file_exists (PUBLIC_PATH . $this->path.'/thumbs/'.$photo)) {
				$ok = false; // si la miniature n'existe pas
			}
			if (!$ok && $make) {
				$this->resizePhoto ($photo, 'thumbs', 400); // on créé la miniature
				$ok = true;
			}

			if (!file_exists (PUBLIC_PATH . $this->path . '/resized/' . $photo)) {
				$ok = false; // même principe, mais en plus grand
			}
			if (!$ok && $make) {
				$this->resizePhoto ($photo, 'resized', 1200);
				$ok = true;
			}
		}

		return $ok;
	}

	public function clearResized () {
		$thumbs = $this->loadThumbs ();
		foreach ($thumbs as $thumb) {
			// si la photo de base n'existe pas
			if (!file_exists (PUBLIC_PATH . $this->path . '/' . $thumb)) {
				unlink (PUBLIC_PATH . $this->path . '/thumbs/' . $thumb);
			}
		}


		$resizeds = $this->loadResized ();
		foreach ($resizeds as $resized) {
			if (!file_exists (PUBLIC_PATH . $this->path . '/' . $resized)) {
				unlink (PUBLIC_PATH . $this->path . '/resized/' . $resized);
			}
		}
	}

	// vérifie si, pour chaque photo de la galerie, celle-ci existe en BD
	// @param $make si à true, créé automatiquement en BD
	// @return true si toutes les photos sont en BD, false sinon
	public function checkPersistence ($make = true) {
		$photoDAO = new PhotoDAO ();
		$ok = true;

		foreach ($this->photos as $photo) {
			$photoTmp = $photoDAO->searchByFileInGalerie ($photo, $this->id ());
			if (!$photoTmp) {
				$ok = false;
			}

			// si pas en bd mais $make à true
			if (!$ok && $make) {
				$num = $photoDAO->countPhotos () + 1;
				$values = array (
					'filePhoto' => $photo,
					'titrePhoto' => $photo,
					'descriptionPhoto' => '',
					'ordrePhoto' => $num,
					'idGalerie' => $this->id
				);

				if ($photoDAO->addPhoto ($values)) {
					$ok = true;
				}
			}
		}

		return $ok;
	}

	public function clearPersistence () {
		$photoDAO = new PhotoDAO ();
		foreach ($this->photosBD as $photo) {
			if (!file_exists (PUBLIC_PATH . $this->path . '/' . $photo->file ())) {
				$photoDAO->deletePhoto ($photo->id ());
			}
		}
	}

	// créé photo redimensionnée
	public function resizePhoto ($photo, $file, $size) {
		$imageSize = getimagesize (PUBLIC_PATH . $this->path . '/' . $photo);

		$width2 = $width = $imageSize[0];
		$height2 = $height = $imageSize[1];
		// on récupère les dimensions finales
		if ($width > $size || $height > $size) {
			$width2 = $height2 = 0;
			if ($width > $height) {
				$width2 = $size;
				$height2 = $height * $width2 / $width;
			} else {
				$height2 = $size;
				$width2 = $width * $height2 / $height;
			}
		}

		// création et redimensionnement de l'image
		$destination = imagecreatetruecolor ($width2, $height2);
		$source = imagecreatefromjpeg (PUBLIC_PATH . $this->path . '/' . $photo);
		imagecopyresized ($destination, $source, 0, 0, 0, 0, $width2, $height2, $width, $height);

		// sauvegarde sur le serveur
		imagejpeg ($destination, PUBLIC_PATH . $this->path . '/' . $file . '/' . $photo);
	}

	// charge les photos dans la variable $photos à partir du $path
	private function loadPhotos () {
		if ($dir = @opendir (PUBLIC_PATH . $this->path)) {
			while (($file = readdir ($dir)) !== false) {
				if (preg_match ("#je?pg$#i", $file)) {
					$this->photos[] = $file;
				}
			}

			closedir ($dir);
		}
	}

	// charge les photos dans la variable $photos à partir de la BD
	private function loadPhotosBD () {
		$photoDAO = new PhotoDAO ();
		$this->photosBD = $photoDAO->listerByGalerie ($this->id);
	}

	// retourne les photos du répertoire thumbs
	private function loadThumbs () {
		$thumbs = array ();

		if ($dir = opendir (PUBLIC_PATH . $this->path . '/thumbs/')) {
			while (($file = readdir ($dir)) !== false) {
				if (preg_match ("#jpg$#i", $file)) {
					$thumbs[] = $file;
				}
			}

			closedir($dir);
		}

		return $thumbs;
	}

	// retourne les photos du répertoire resized
	private function loadResized () {
		$resized = array ();

		if ($dir = opendir (PUBLIC_PATH . $this->path . '/resized/')) {
			while (($file = readdir ($dir)) !== false) {
				if (preg_match ("#jpg$#i",$file)) {
					$resized[] = $file;
				}
			}

			closedir ($dir);
		}

		return $resized;
	}

	// vérifie l'ordre des photos en BD (pour éviter les trous)
	public function checkOrder () {
		$photoDAO = new PhotoDAO ();
		$photos = $photoDAO->lister ();

		$i = 1;

		foreach ($photos as $photo) {
			if ($photo->ordre () != $i) {
				$values = array (
					'filePhoto' => $photo->file (),
					'titrePhoto' => $photo->titre (),
					'descriptionPhoto' => $photo->description (),
					'ordrePhoto' => $i
				);

				$photoDAO->updatePhoto ($photo->id (), $values);
			}

			$i++;
		}
	}
}

class GalerieDAO extends Model_pdo {
	public function __construct () {
		parent::__construct ();
	}

	public function lister () {
		$sql = 'SELECT * FROM galerie';
		$stm = $this->bd->prepare ($sql); 
		$stm->execute ();

		return HelperGalerie::listeDaoToGalerie ($stm->fetchAll (PDO::FETCH_CLASS));
	}

	public function searchById ($id) {
		$sql = 'SELECT * FROM galerie WHERE idGalerie=?';
		$values = array ($id);
		$stm = $this->bd->prepare ($sql); 
		$stm->execute ($values);
		$res = $stm->fetchAll (PDO::FETCH_CLASS);

		if (!empty ($res)) {
			return HelperGalerie::daoToGalerie ($res[0]);
		} else {
			return false;
		}
	}

	public function addGalerie ($valuesTmp) {
		$sql = 'INSERT INTO galerie(nomGalerie, descriptionGalerie, pathGalerie) VALUES(?,?,?)';
		$stm = $this->bd->prepare ($sql);

		$values = array (
			$valuesTmp['nomGalerie'],
			$valuesTmp['descriptionGalerie'],
			$valuesTmp['pathGalerie']
		);

		if ($stm && $stm->execute ($values)) {
			return true;
		} else {
			return false;
		}
	}

	public function updateGalerie ($id, $valuesTmp) {
		$sql = 'UPDATE galerie SET nomGalerie=?, descriptionGalerie=?, pathGalerie=? WHERE idGalerie=?';
		$stm = $this->bd->prepare ($sql);

		$values = array (
			$valuesTmp['nomGalerie'],
			$valuesTmp['descriptionGalerie'],
			$valuesTmp['pathGalerie'],
			$id
		);

		if ($stm && $stm->execute ($values)) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteGalerie ($id) {
		$photoDAO = new PhotoDAO ();
		if (!$photoDAO->deleteByGalerie($id)) {
			return false;
		}

		$sql = 'DELETE FROM galerie WHERE idGalerie=?';
		$stm = $this->bd->prepare ($sql);
		$values = array ($id);

		if ($stm && $stm->execute ($values)) {
			return true;
		} else {
			return false;
		}
	}
}

class HelperGalerie {
	public static function daoToGalerie ($dao) {
		if (!is_array ($dao)) {
			$dao = array ($dao);
		}
		
		$galeries = HelperGalerie::listeDaoToGalerie ($dao);

		return $galeries[0];
	}
    
	public static function listeDaoToGalerie ($listeDAO) {
		$liste = array ();

		foreach ($listeDAO as $key => $dao) {
			$liste[$key] = new Galerie ($dao->nomGalerie,
			                            $dao->pathGalerie,
			                            $dao->idGalerie);
			$liste[$key]->_description ($dao->descriptionGalerie);
		}

		return $liste;
	}
}
