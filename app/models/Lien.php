<?php

class Lien extends Model {
	private $id;
	private $url;
	private $libelle;

	public function id () { return $this->id; }
	public function url () { return $this->url; }
	public function libelle () { return $this->libelle; }
	public function _id ($id) { $this->id = $id; }
	public function _url ($url) { $this->url = $url; }
	public function _libelle ($libelle) { $this->libelle = $libelle; }
}

class LienDAO extends Model_pdo {
	public function __construct () {
		parent::__construct ();
	}

	public function lister () {
		$sql = 'SELECT * FROM lien';
		$stm = $this->bd->prepare ($sql); 
		$stm->execute ();

		return HelperLien::listeDaoToLien ($stm->fetchAll (PDO::FETCH_CLASS));
	}

	public function searchById ($id) {
		$sql = 'SELECT * FROM lien WHERE idLien=?';
		$values = array ($id);
		$stm = $this->bd->prepare ($sql); 
		$stm->execute ($values);
		$res = $stm->fetchAll (PDO::FETCH_CLASS);

		if (!empty ($res)) {
			return HelperLien::daoToLien ($res[0]);
		} else {
			return false;
		}
	}

	public function addLien ($valuesTmp) {
		$sql = 'INSERT INTO lien(urlLien, libelleLien) VALUES(?,?)';
		$stm = $this->bd->prepare ($sql);

		$values = array (
			$valuesTmp['urlLien'],
			$valuesTmp['libelleLien']
		);

		if ($stm && $stm->execute ($values)) {
			return true;
		} else {
			return false;
		}
	}

	public function updateLien($id, $valuesTmp) {
		$sql = 'UPDATE lien SET urlLien=?, libelleLien=? WHERE idLien=?';
		$stm = $this->bd->prepare ($sql);

		$values = array (
			$valuesTmp['urlLien'],
			$valuesTmp['libelleLien'],
			$id
		);

		if ($stm && $stm->execute ($values)) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteLien ($id) {
		$sql = 'DELETE FROM lien WHERE idLien=?';
		$stm = $this->bd->prepare ($sql); 

		$values = array ($id);

		if ($stm && $stm->execute ($values)) {
			return true;
		} else {
			return false;
		}
	}
}

class HelperLien {
	public static function daoToLien ($dao) {
		if (!is_array ($dao)) {
			$dao = array ($dao);
		}
		
		$liens = HelperLien::listeDaoToLien ($dao);

		return $liens[0];
	}
    
	public static function listeDaoToLien ($listeDAO) {
		$liste = array ();
		
		foreach ($listeDAO as $key => $dao) {
			$liste[$key] = new Lien ();
			$liste[$key]->_id ($dao->idLien);
			$liste[$key]->_url ($dao->urlLien);
			$liste[$key]->_libelle ($dao->libelleLien);
		}

		return $liste;
	}
}
