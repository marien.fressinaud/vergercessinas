<?php

class Article extends Model {
	private $id = 0;
	private $title = '';
	private $content = '';
	private $date = '';
	private $level = 1;

	public static $levels = array ('important', 'information', 'conseil');

	public function id () { return $this->id; }
	public function title () { return $this->title; }
	public function content () { return $this->content; }
	public function mdContent () {
		$Parsedown = new ParsedownExtra();
		$Parsedown->setBreaksEnabled(true);
		return $Parsedown->text($this->content);
	}
	public function date () { return $this->date; }
	public function level () { return $this->level; }
	public function _id ($id) { $this->id = $id; }
	public function _title ($title) { $this->title = $title; }
	public function _content ($content) { $this->content = $content; }
	public function _date ($date) { $this->date = $date; }
	public function _level ($level) { $this->level = $level; }
}

class ArticleDAO extends Model_pdo {
	public function __construct () {
		parent::__construct ();
	}

	public function lister () {
		$sql = 'SELECT * FROM article ORDER BY dateArticle DESC';
		$stm = $this->bd->prepare ($sql); 
		$stm->execute ();

		return HelperArticle::listeDaoToArticle ($stm->fetchAll (PDO::FETCH_CLASS));
	}

	public function searchById ($id) {
		$sql = 'SELECT * FROM article WHERE idArticle=?';
		$values = array ($id);
		$stm = $this->bd->prepare ($sql); 
		$stm->execute ($values);
		$res = $stm->fetchAll (PDO::FETCH_CLASS);

		if (!empty ($res)) {
			return HelperArticle::daoToArticle ($res[0]);
		} else {
			return false;
		}
	}

	public function searchLastArticle () {
		$sql = 'SELECT * FROM article ORDER BY dateArticle DESC';
		$stm = $this->bd->prepare ($sql); 
		$stm->execute ();
		$res = $stm->fetchAll (PDO::FETCH_CLASS);

		if (!empty ($res)) {
			return HelperArticle::daoToArticle ($res[0]);
		} else {
			return false;
		}
	}

	public function addArticle ($valuesTmp) {
		$sql = 'INSERT INTO article(titleArticle, contentArticle, dateArticle, levelArticle) VALUES(?,?,?,?)';
		$stm = $this->bd->prepare ($sql);

		$values = array (
			$valuesTmp['titleArticle'],
			$valuesTmp['contentArticle'],
			$valuesTmp['dateArticle'],
			$valuesTmp['levelArticle']
		);

		if ($stm && $stm->execute ($values)) {
			return true;
		} else {
			return false;
		}
	}

	public function updateArticle ($id, $valuesTmp) {
		$sql = 'UPDATE article SET titleArticle=?, contentArticle=?, dateArticle=?, levelArticle=? WHERE idArticle=?';
		$stm = $this->bd->prepare ($sql);

		$values = array (
			$valuesTmp['titleArticle'],
			$valuesTmp['contentArticle'],
			$valuesTmp['dateArticle'],
			$valuesTmp['levelArticle'],
			$id
		);

		if ($stm && $stm->execute ($values)) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteArticle ($id) {
		$sql = 'DELETE FROM article WHERE idArticle=?';
		$stm = $this->bd->prepare ($sql); 

		$values = array ($id);

		if ($stm && $stm->execute ($values)) {
			return true;
		} else {
			return false;
		}
	}

	public function countArticles () {
		$sql = 'SELECT COUNT(*) AS nombreArticles FROM article';
		$stm = $this->bd->prepare ($sql); 
		$stm->execute ();
		$res = $stm->fetchAll (PDO::FETCH_CLASS);

		if (!empty ($res)) {
			return $res[0]->nombreArticles;
		} else {
			return 0;
		}
	}
}

class HelperArticle {
	public static function daoToArticle ($dao) {
		if (!is_array ($dao)) {
			$dao = array ($dao);
		}
		
		$articles = HelperArticle::listeDaoToArticle ($dao);

		return $articles[0];
	}
    
	public static function listeDaoToArticle ($listeDAO) {
		$liste = array ();

		foreach ($listeDAO as $key => $dao) {
			$liste[$key] = new Article();
			$liste[$key]->_id ($dao->idArticle);
			$liste[$key]->_title ($dao->titleArticle);
			$liste[$key]->_content ($dao->contentArticle);
			$liste[$key]->_date ($dao->dateArticle);
			$liste[$key]->_level ($dao->levelArticle);
		}

		return $liste;
	}
}
