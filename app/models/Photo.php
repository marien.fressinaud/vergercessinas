<?php

class Photo extends Model {
	private $id;
	private $file;
	private $titre;
	private $description;
	private $ordre;
	private $idGalerie;

	public function id () { return $this->id; }
	public function file () { return $this->file; }
	public function titre () { return $this->titre; }
	public function description () { return $this->description; }
	public function ordre () { return $this->ordre; }
	public function idGalerie () { return $this->idGalerie; }
	public function galeriePath () {
		$galerieDAO = new GalerieDAO ();
		$galerie = $galerieDAO->searchById ($this->idGalerie);

		return $galerie->path ();
	}
	public function _id ($id) { $this->id = $id; }
	public function _file ($file) { $this->file = $file; }
	public function _titre ($titre) { $this->titre = $titre; }
	public function _description ($description) { $this->description = $description; }
	public function _ordre ($ordre) { $this->ordre = $ordre; }
	public function _idGalerie ($idGalerie) { $this->idGalerie = $idGalerie; }

	public function deleteFile () {
		$filepath = PUBLIC_PATH . $this->galeriePath () . '/' . $this->file;
		return unlink($filepath);
	}
}

class PhotoDAO extends Model_pdo {
	public function __construct() {
		parent::__construct();
	}

	public function lister () {
		$sql = 'SELECT * FROM photo ORDER BY ordrePhoto';
		$stm = $this->bd->prepare ($sql); 
		$stm->execute ();

		return HelperPhoto::listeDaoToPhoto ($stm->fetchAll (PDO::FETCH_CLASS));
	}

	public function listerByGalerie ($id) {
		$sql = 'SELECT * FROM photo WHERE idGalerie=? ORDER BY ordrePhoto';
		$values = array ($id);
		$stm = $this->bd->prepare ($sql); 
		$stm->execute ($values);

		return HelperPhoto::listeDaoToPhoto ($stm->fetchAll(PDO::FETCH_CLASS));
	}

	public function searchById ($id) {
		$sql = 'SELECT * FROM photo WHERE idPhoto=?';
		$values = array ($id);
		$stm = $this->bd->prepare ($sql); 
		$stm->execute ($values);
		$res = $stm->fetchAll (PDO::FETCH_CLASS);

		if (!empty ($res)) {
			return HelperPhoto::daoToPhoto ($res[0]);
		} else {
			return false;
		}
	}

	public function searchByOrdre ($num) {
		$sql = 'SELECT * FROM photo WHERE ordrePhoto=?';
		$values = array ($num);
		$stm = $this->bd->prepare ($sql); 
		$stm->execute ($values);
		$res = $stm->fetchAll (PDO::FETCH_CLASS);

		if (!empty ($res)) {
			return HelperPhoto::daoToPhoto ($res[0]);
		} else {
			return false;
		}
	}

	public function searchByFileInGalerie ($file, $idGalerie) {
		$sql = 'SELECT * FROM photo WHERE filePhoto=? AND idGalerie=?';
		$values = array ($file, $idGalerie);
		$stm = $this->bd->prepare ($sql);
		$stm->execute ($values);
		$res = $stm->fetchAll (PDO::FETCH_CLASS);

		if (!empty ($res)) {
			return HelperPhoto::daoToPhoto ($res[0]);
		} else {
			return false;
		}
	}

	public function searchFirstPhoto () {
		$sql = 'SELECT * FROM photo ORDER BY ordrePhoto';
		$stm = $this->bd->prepare ($sql); 
		$stm->execute ();
		$res = $stm->fetchAll (PDO::FETCH_CLASS);

		if (!empty ($res)) {
			return HelperPhoto::daoToPhoto ($res[0]);
		} else {
			return false;
		}
	}

	public function searchLastPhoto () {
		$sql = 'SELECT * FROM photo ORDER BY ordrePhoto DESC';
		$stm = $this->bd->prepare ($sql); 
		$stm->execute ();
		$res = $stm->fetchAll (PDO::FETCH_CLASS);

		if (!empty ($res)) {
			return HelperPhoto::daoToPhoto ($res[0]);
		} else {
			return false;
		}
	}

	public function addPhoto ($valuesTmp) {
		$sql = 'INSERT INTO photo(filePhoto, titrePhoto, descriptionPhoto, ordrePhoto, idGalerie) VALUES(?,?,?,?,?)';
		$stm = $this->bd->prepare ($sql);

		$values = array (
			$valuesTmp['filePhoto'],
			$valuesTmp['titrePhoto'],
			$valuesTmp['descriptionPhoto'],
			$valuesTmp['ordrePhoto'],
			$valuesTmp['idGalerie']
		);

		if ($stm && $stm->execute ($values)) {
			return true;
		} else {
			return false;
		}
	}

	public function updatePhoto ($id, $valuesTmp) {
		$sql = 'UPDATE photo SET titrePhoto=?, descriptionPhoto=?, ordrePhoto=?, idGalerie=? WHERE idPhoto=?';
		$stm = $this->bd->prepare ($sql);

		$values = array (
			$valuesTmp['titrePhoto'],
			$valuesTmp['descriptionPhoto'],
			$valuesTmp['ordrePhoto'],
			$valuesTmp['idGalerie'],
			$id
		);

		if ($stm && $stm->execute ($values)) {
			return true;
		} else {
			return false;
		}
	}

	public function deletePhoto ($id) {
		$sql = 'DELETE FROM photo WHERE idPhoto=?';
		$stm = $this->bd->prepare ($sql); 

		$values = array ($id);

		if ($stm && $stm->execute ($values)) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteByGalerie ($id) {
		$sql = 'DELETE FROM photo WHERE idGalerie=?';
		$stm = $this->bd->prepare ($sql);

		$values = array ($id);

		if ($stm && $stm->execute ($values)) {
			return true;
		} else {
			return false;
		}
	}

	public function countPhotos () {
		$sql = 'SELECT COUNT(*) AS nombrePhotos FROM photo';
		$stm = $this->bd->prepare ($sql); 
		$stm->execute ();
		$res = $stm->fetchAll (PDO::FETCH_CLASS);

		
		if (!empty ($res)) {
			return $res[0]->nombrePhotos;
		} else {
			return 0;
		}
	}
}

class HelperPhoto {
	public static function daoToPhoto ($dao) {
		if (!is_array ($dao)) {
			$dao = array ($dao);
		}
		
		$photos = HelperPhoto::listeDaoToPhoto ($dao);

		return $photos[0];
	}

	public static function listeDaoToPhoto ($listeDAO) {
		$liste = array ();

		foreach ($listeDAO as $key => $dao) {
			$liste[$key] = new Photo ();
			$liste[$key]->_id ($dao->idPhoto);
			$liste[$key]->_file ($dao->filePhoto);
			$liste[$key]->_titre ($dao->titrePhoto);
			$liste[$key]->_description ($dao->descriptionPhoto);
			$liste[$key]->_ordre ($dao->ordrePhoto);
			$liste[$key]->_idGalerie ($dao->idGalerie);
		}

		return $liste;
	}
}
