<?php

function is_admin () {
	return Session::param ('isConnected');
}

class User extends Model {
	private $username = '';
	private $mail = '';
	private $password = '';
	
	public function username () { return $this->username; }
	public function mail () { return $this->mail; }
	public function password () { return $this->password; }
	public function _username ($username) { $this->username = $username; }
	public function _mail ($mail) { $this->mail = $mail; }
	public function _password ($password) { $this->password = $password; }
	
	public function login ($pass) {
        $userDAO = new UserDAO ();
        
		if ($userDAO->checkPassword (sha1($pass))) {
			Session::_param ('isConnected', true);
		}
	}
	
	public function logout () {
		Session::_param ('isConnected');
	}
}

class UserDAO extends Model_pdo {
	public function __construct () {
		parent::__construct ();
	}
	
	public function searchByUsername ($username) {
		$sql = 'SELECT * FROM user WHERE loginUser=?';
		$values = array ($username);
		$stm = $this->bd->prepare ($sql); 
		$stm->execute ($values);
		$res = $stm->fetchAll (PDO::FETCH_CLASS);

		if (!empty ($res)) {
			return HelperUser::daoToUser ($res[0]);
		} else {
			return false;
		}
	}

	public function searchMainUser () {
		$sql = 'SELECT * FROM user';
		$values = array ();
		$stm = $this->bd->prepare ($sql); 
		$stm->execute ($values);
		$res = $stm->fetchAll (PDO::FETCH_CLASS);

		if (!empty ($res)) {
			return HelperUser::daoToUser ($res[0]);
		} else {
			return false;
		}
	}
	
	public function checkPassword ($pass) {
		$sql = 'SELECT * FROM user WHERE passUser=?';
		$values = array ($pass);
		$stm = $this->bd->prepare ($sql); 
		$stm->execute ($values);
		$res = $stm->fetchAll ();

		if ($stm && !empty ($res)) {
			return true;
		} else {
			return false;
		}
	}

	public function updateUser ($username, $valuesTmp) {
		$sql = 'UPDATE user SET mailUser=?, passUser=? WHERE loginUser=?';
		$stm = $this->bd->prepare ($sql);

		$values = array (
			$valuesTmp['mailUser'],
			$valuesTmp['passUser'],
			$username
		);

		if ($stm && $stm->execute ($values)) {
			return true;
		} else {
			return false;
		}
	}

	public function createUser ($username, $password) {
		$sql = 'INSERT INTO user(loginUser, passUser) VALUES(?,?)';
		$stm = $this->bd->prepare ($sql);
		$values = array (
			$username,
			$password
		);

		if ($stm && $stm->execute ($values)) {
			return true;
		} else {
			return false;
		}
	}
}

class HelperUser {
	public static function daoToUser ($dao) {
		if (!is_array ($dao)) {
			$dao = array ($dao);
		}
		
		$liens = HelperUser::listeDaoToUser ($dao);

		return $liens[0];
	}

	public static function listeDaoToUser ($listeDAO) {
		$liste = array ();

		foreach ($listeDAO as $key => $dao) {
			$liste[$key] = new User ();
			$liste[$key]->_username ($dao->loginUser);
			$liste[$key]->_mail ($dao->mailUser);
			$liste[$key]->_password ($dao->passUser);
		}

		return $liste;
	}
}
