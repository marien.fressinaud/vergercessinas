<?php
  
class adminController extends ActionController {
	public function firstAction () {
		$this->view->_layout('admin');
		View::appendStyle (Url::display ('/themes/default/admin.css'));
	}

	public function indexAction () {
		if (!is_admin ()) {
			MinzError::error (
				403,
				array ('error' => array ('Vous n\'avez pas le droit d\'accéder à cette page'))
			);
		}
		
		$info = Request::param ('info');
		if ($info == 1) {
			$this->view->ok = 'Vous êtes désormais connecté';
		}
		
		View::prependTitle('Panel d\'administration - ');
	}
	
	public function userAction () {
		if (!is_admin ()) {
			MinzError::error (
				403,
				array ('error' => array ('Vous n\'avez pas le droit d\'accéder à cette page'))
			);
		}
		
		$userDAO = new UserDAO ();
		$this->view->user = $userDAO->searchMainUser ();
		
		if (Request::isPost ()) {
			$ok = true;
			
			$mail = Request::param ('mail', '');
			$pass = Request::param ('pass');
			$confirmePass = Request::param ('confirmePass');

			if (!empty ($mail)) {
				if (!filter_var ($mail, FILTER_VALIDATE_EMAIL)) {
					$ok = false;
					$this->view->error = 'Le format de l\'adresse mail est invalide';
				} else {
					$this->view->user->_mail ($mail);
				}
			}
			
			if (!empty ($pass) || !empty ($confirmePass)) {
				if ($pass != $confirmePass) {
					$ok = false;
					$this->view->error = 'Les deux mots de passe ne correspondent pas';
				}
			}

			if ($ok) {
				$values = array (
					'mailUser' => $mail,
				);
				// we want to change the password only if the user filled
				// the input
				if (!empty($pass)) {
					$values['passUser'] = sha1 ($pass);
				} else {
					$values['passUser'] = $this->view->user->password ();
				}
				$username = $this->view->user->username ();

				if ($userDAO->updateUser ($username, $values)) {
					$this->view->ok = 'Vos modifications ont bien été prises en compte';
				} else {
					$this->view->error = 'Un problème est survenu lors de la modification';
				}
			}
		}
		
		View::prependTitle('Gestion du profil utilisateur - ');
	}

	public function loginAction () {
		if (is_admin ()) {
			Request::forward (array (
				'c' => 'admin'
			), true);
		}
		
		if (Request::isPost ()) {
			$pass = Request::param ('password');
			if (!empty ($pass)) {
				$user = new User ();
				$user->login ($pass);
				
				if (is_admin ()) {
					Request::forward (array (
						'c' => 'admin',
						'a' => 'index'
					), true);
				} else {
					$this->view->error = 'Votre mot de passe est erroné';
				}
			} else {
				$this->view->error = 'Veuillez indiquer un mot de passe';
			}
		}
		
		View::prependTitle('Connexion - ');
	}
	
	public function logoutAction () {
		$user = new User ();
		$user->logout ();
		Request::forward (array (
			'c' => 'admin',
			'a' => 'login'
		), true);
	}
}
