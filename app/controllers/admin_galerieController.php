<?php

class admin_galerieController extends ActionController {
	public function firstAction() {
		$this->view->_layout('admin');
		View::appendStyle (Url::display ('/themes/default/admin.css'));

		if (!is_admin ()) {
			MinzError::error (
				403,
				array ('warning' => array ('Vous n\'avez pas le droit d\'accéder à cette page'))
			);
		}
	}

	public function indexAction () {
		$galerieDAO = new GalerieDAO ();
		$this->view->galeries = $galerieDAO->lister ();

		View::prependTitle ('Gestion des galeries - ');
	}

	public function addAction() {
		View::prependTitle ('Ajouter une galerie - ');

		if (Request::isPost ()) {
			$nom = htmlspecialchars (Request::param ('nom', ''));
			$desc = htmlspecialchars (Request::param ('description', ''));

			if (!empty ($nom)) {
				$galerieDAO = new GalerieDAO ();

				$values = array (
					'nomGalerie' => $nom,
					'descriptionGalerie' => $desc,
					'pathGalerie' => '/galerie/' . filterChar ($nom)
				);

				if ($galerieDAO->addGalerie ($values)) {
					$this->view->ok = 'La galerie a bien été ajoutée';
				} else {
					$this->view->error = 'Un problème est survenu lors de la création de la galerie ! Vérifiez qu\'aucune galerie n\'utilise déjà le chemin <strong>'.$values['pathGalerie'].'</strong>';
				}
			} else {
				$this->view->error = 'Tous les champs ne sont pas remplis correctement';
			}
		}
	}
	public function updateAction() {
		View::prependTitle ('Modifier une galerie - ');

		$galerieDAO = new GalerieDAO ();

		$idGalerie = Request::param ('id');

		if ($idGalerie !== false) {
			if (Request::isPost ()) {
				$nom = htmlspecialchars (Request::param ('nom', ''));
				$desc = htmlspecialchars (Request::param ('description', ''));
				$path = htmlspecialchars (Request::param ('path', ''));

				if (!empty ($nom) && !empty ($path)) {
					$values = array (
						'nomGalerie' => $nom,
						'descriptionGalerie' => $desc,
						'pathGalerie' => $path
					);

					if ($galerieDAO->updateGalerie ($idGalerie, $values)) {
						$this->view->ok = 'La galerie a bien été modifiée';
					} else {
						$this->view->error = 'Un problème est survenu lors de la mise à jour de la galerie ! Vérifiez qu\'aucune galerie n\'utilise déjà le chemin <strong>'.$values['pathGalerie'].'</strong>';
					}
				} else {
					$this->view->error = 'Tous les champs ne sont pas remplis correctement';
				}
			}

			$galerie = $galerieDAO->searchById ($idGalerie);
			if ($galerie !== false) {
				$this->view->galerie = $galerie;
			} else {
				MinzError::error (
					404,
					array ('error' => array ('La page que vous cherchez n\'existe pas'))
				);
			}
		} else {
			MinzError::error (
				404,
				array ('error' => array ('La page que vous cherchez n\'existe pas'))
			);
		}
	}

	public function deleteAction () {
		$idGalerie = intval (Request::param ('id', '0'));

		if ($idGalerie !== 0) {
			$galerieDAO = new GalerieDAO ();

			if ($galerieDAO->deleteGalerie ($idGalerie)) {
				$this->view->ok = 'La galerie a bien été supprimée';
			} else {
				$this->view->error = 'Un problème est survenu lors de la suppression de la galerie';
			}
		} else {
			MinzError::error (
				404,
				array ('error' => array ('La page que vous cherchez n\'existe pas'))
			);
		}
	}

	public function check_galerieAction () {
		$idGalerie = Request::param ('id');

		if ($idGalerie !== false) {
			$galerieDAO = new GalerieDAO ();
			$galerie = $galerieDAO->searchById ($idGalerie);

			$galerie->checkResized ();
			$galerie->clearResized ();
			$galerie->checkPersistence (true);
			$galerie->clearPersistence ();
			$galerie->checkOrder ();

			$this->view->ok = 'Galerie mise à jour !';
			Request::forward (array (
				'c' => 'admin_photo',
				'a' => 'index',
				'params' => array (
					'id' => $galerie->id (),
				),
			), true);
		} else {
			MinzError::error (
				404,
				array ('error' => array ('La page que vous cherchez n\'existe pas'))
			);
		}
	}

	public function upload_photoAction () {
		$idGalerie = Request::param ('id');
		if ($idGalerie === false) {
			MinzError::error (
				404,
				array ('error' => array ('La page que vous cherchez n\'existe pas'))
			);
			return;
		}

		$galerieDAO = new GalerieDAO ();
		$galerie = $galerieDAO->searchById ($idGalerie);

		$uploaded_photo = $_FILES['photo'];
		$photo_filename = PUBLIC_PATH . $galerie->path () . DIRECTORY_SEPARATOR . $uploaded_photo['name'];
		if (move_uploaded_file($uploaded_photo['tmp_name'], $photo_filename)) {
			Request::forward (array (
				'c' => 'admin_galerie',
				'a' => 'check_galerie',
				'params' => array (
					'id' => $galerie->id (),
				),
			));
		} else {
			MinzError::error (
				500,
				array ('error' => array ('La photo n’a pas pu être enregistré'))
			);
		}
	}
}
