<?php

class indexController extends ActionController {
	public function indexAction () {
		$pageDAO = new PageDAO ();
		$idPage = Request::param ('page', 1);

		$page = $pageDAO->searchById ($idPage);
		if ($page === false) {
			// no page? So we need to create one. Let's redirect to the
			// administration.
			$url = array(
				'c' => 'admin',
				'a' => 'login',
			);
			Request::forward ($url, true);
		} else {
			$this->view->page = $page;

			View::appendStyle (Url::display ('/themes/default/base.css'));
			View::prependTitle ($page->title () . ' - ');
		}
	}
}
