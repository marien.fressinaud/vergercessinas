# vergercessinas

Sources du site [levergerdecessinas.fr](https://levergerdecessinas.fr).
Ce site a été développé il y a beaucoup trop de temps pour que je tente de me
souvenir de la date. Le code est celui d'un étudiant en DUT en informatique,
avec un cadriciel fait maison, merci de votre indulgence quant au code produit <3

## Environnement technique

Ce site tourne sur une base PHP. Je ne compte pas le maintenir si ce n'est pour
des raisons de sécurité. Ce site sera refait. Un jour.

## Développement et production

Pour configurer l'environnement qui va bien, on ne va pas s'enquiquiner à
installer un serveur LAMP ou WAMP comme à la belle époque. Docker est là pour
nous simplifier la vie (et oui je t'entends râler toi là-bas, mais as-tu
_vraiment_ envie d'installer un environnement PHP sur ta machine ?)

Je vous laisse installer le nécessaire : [Docker](https://docs.docker.com/install/).

Ensuite, je suis sympa, j'ai tout automatisé. Si vous voulez lancer le projet,
lancez simplement :

```console
$ make docker-start
```

Si tout va bien, vous devriez pouvoir accéder à un site vide à l'adresse
[localhost:8000](http://localhost:8000). Le mot de passe par défaut est
`my-secret`.

La commande `make` ci-dessus se contente d'exécuter une commande
`docker compose` en prenant en paramètre le fichier se trouvant dans le
répertoire `./docker`. Deux services sont lancés : `php` qui se charge
d'interpréter les requêtes PHP et `nginx` qui sert à la fois les fichiers
statiques et sert de proxy pour rebalancer les requêtes sur le fichier
`index.php` vers le service `php`. Voir pour cela le fichier `./docker/nginx.conf`
qui précise un peu tout ça.

Deux variables d'environnement sont aussi définies dans le `docker-compose.yml` :

- `APP_ENVIRONMENT` par défaut à `development`, à modifier sur `production` le
  cas échéant
- `APP_SECRET_KEY` qui devrait idéalement rester secrète et donc à changer en
  production (honnêtement le niveau de sécurité de ce site est à pleurer, ça
  changera pas grand chose)

Notez enfin que le service `php` a besoin de prende l'UID et le GID de
l'utilisateur courant pour avoir les droits qu'il faut pour écrire dans la base
de données. La variable d'environnement est créée dans le `Makefile` (c'est
d'ailleurs sa seule véritable utilité).

Vous n'avez plus qu'à coder et corriger les bugs et failles de sécurité !
