<?php

define ('ROOT_PATH', dirname (__FILE__));
define ('DATA_PATH', ROOT_PATH . DIRECTORY_SEPARATOR . 'data');
define ('MIGRATIONS_PATH', ROOT_PATH . DIRECTORY_SEPARATOR . 'migrations');
define ('DATABASE_FILE', DATA_PATH . DIRECTORY_SEPARATOR . 'db.sqlite');
define ('DATABASE_VERSION_FILE', DATA_PATH . DIRECTORY_SEPARATOR . 'version.txt');

function get_db_version() {
	if (file_exists(DATABASE_VERSION_FILE)) {
		$file_content = file_get_contents(DATABASE_VERSION_FILE);
		return (int) trim($file_content);
	}
	return 0;
}

function update_db_version($new_version) {
	return file_put_contents(DATABASE_VERSION_FILE, $new_version);
}

function get_migration_version($migration) {
	return (int) filter_var(get_class($migration), FILTER_SANITIZE_NUMBER_INT);
}

function list_migrations() {
	$migrations = array();
	$migration_files = scandir(MIGRATIONS_PATH);
	foreach ($migration_files as $file) {
		if ($file === '.' || $file === '..') {
			continue;
		}

		$matches = [];
		preg_match('/^migration_(\d)+\.php$/', $file, $matches);
		$migration_version = $matches[1];

		require(MIGRATIONS_PATH . DIRECTORY_SEPARATOR . $file);
		$migration_class = 'Migration' . $migration_version;
		$migrations[] = new $migration_class();
	}
	return $migrations;
}

function list_migrations_to_apply($db_version) {
	$migrations_to_apply = array();
	if ($db_version === 0) {
		$last_applied_migration_found = true;
	} else {
		$last_applied_migration_found = false;
	}

	foreach (list_migrations() as $migration) {
		$versions_match = $db_version === get_migration_version($migration);
		$last_applied_migration_found = $last_applied_migration_found || $versions_match;
		if (!$last_applied_migration_found || $versions_match) {
			continue;
		}
		$migrations_to_apply[] = $migration;
	}

	return $migrations_to_apply;
}

$db = new PDO('sqlite:' . DATABASE_FILE);

$db_version = get_db_version();
echo "Current DB version: ", $db_version, "\n";

$migrations_to_apply = list_migrations_to_apply($db_version);
if (count($migrations_to_apply) === 0) {
	echo "No migrations to apply\n";
	return;
}

foreach ($migrations_to_apply as $migration) {
	$migration_version = get_migration_version($migration);
	echo "Applying migration ", $migration_version, "... ";
	try {
		$migration->up($db);
		update_db_version($migration_version);
		echo "OK\n";
	} catch (Exception $e) {
		echo "Fail\n";
		throw $e;
	}
}
