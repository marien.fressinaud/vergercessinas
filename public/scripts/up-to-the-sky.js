(function () {
    var backToTopContainer = document.querySelector('#up-to-the-sky');
    var intersectionObserver = new IntersectionObserver(function (entries) {
        entries.forEach(function (entry) {
            entry.target.classList.toggle('intersecting', entry.isIntersecting);
        });
    });
    intersectionObserver.observe(backToTopContainer);

    var backToTopButton = backToTopContainer.querySelector('button');
    backToTopButton.onclick = function() {
        window.scrollTo({ top: 0, behavior: 'smooth' });
    };
}());
